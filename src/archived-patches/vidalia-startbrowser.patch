Index: src/vidalia/browserprocess.cpp
===================================================================
--- src/vidalia/browserprocess.cpp	(revision 0)
+++ src/vidalia/browserprocess.cpp	(revision 0)
@@ -0,0 +1,32 @@
+/**
+ ** Test invoking Firefox from Qt
+ ** Steven J. Murdoch <http://www.cl.cam.ac.uk/users/sjm217/>
+ ** $Id: browserprocess.cpp 12682 2007-12-05 17:02:21Z sjm217 $
+ **/
+
+#include <QString>
+  
+#include "browserprocess.h"
+
+BrowserProcess::BrowserProcess(QObject *parent)
+: QProcess(parent)
+{
+  // Call error handling routine on errors
+  QObject::connect(this, SIGNAL(error(QProcess::ProcessError)), this, SLOT(onError(QProcess::ProcessError)));
+}
+
+void
+BrowserProcess::start(QString app, QStringList args) 
+{
+  // Start the specified application
+  QProcess::start(app, args, QIODevice::ReadOnly | QIODevice::Text);
+}
+
+void
+BrowserProcess::onError(QProcess::ProcessError error)
+{
+  // Pass up error messages on startup, but ignore the rest
+  if (error == QProcess::FailedToStart) {
+    emit startFailed(errorString());
+  }
+}
Index: src/vidalia/browserprocess.h
===================================================================
--- src/vidalia/browserprocess.h	(revision 0)
+++ src/vidalia/browserprocess.h	(revision 0)
@@ -0,0 +1,31 @@
+/**
+ ** Test invoking Firefox from Qt
+ ** Steven J. Murdoch <http://www.cl.cam.ac.uk/users/sjm217/>
+ ** $Id: browserprocess.h 12682 2007-12-05 17:02:21Z sjm217 $
+ **/
+
+#ifndef _BROWSERPROCESS_H
+#define _BROWSERPROCESS_H
+
+#include <QProcess>
+
+class BrowserProcess : public QProcess
+{
+  Q_OBJECT
+
+public:
+  // Default constructor
+  BrowserProcess(QObject *parent = 0);
+  // Start the specified application
+  void start(QString app, QStringList args);
+
+private slots:
+  // Invoked when underlying QProcess fails
+  void onError(QProcess::ProcessError error);
+
+signals:
+  // Invoked when start() fails
+  void startFailed(QString errorMessage);
+};
+
+#endif
Index: src/vidalia/CMakeLists.txt
===================================================================
--- src/vidalia/CMakeLists.txt	(revision 2205)
+++ src/vidalia/CMakeLists.txt	(working copy)
@@ -176,6 +176,7 @@
   vclicklabel.cpp
   vidaliawindow.cpp
   vmessagebox.cpp
+  browserprocess.cpp
 )
 qt4_wrap_cpp(vidalia_SRCS
   vidalia.h
@@ -184,6 +185,7 @@
   vclicklabel.h
   vidaliawindow.h
   vmessagebox.h
+  browserprocess.h
 )
 
 ## Specify all the Qt Designer .ui files
Index: src/vidalia/config/vidaliasettings.cpp
===================================================================
--- src/vidalia/config/vidaliasettings.cpp	(revision 2205)
+++ src/vidalia/config/vidaliasettings.cpp	(working copy)
@@ -42,6 +42,7 @@
 #define SETTING_RUN_TOR_AT_START    "RunTorAtStart"
 #define SETTING_DATA_DIRECTORY      "DataDirectory"
 #define SETTING_SHOW_MAINWINDOW_AT_START  "ShowMainWindowAtStart"
+#define SETTING_BROWSER_EXECUTABLE  "BrowserExecutable"
 
 #if defined(Q_OS_WIN32)
 #define STARTUP_REG_KEY        "Software\\Microsoft\\Windows\\CurrentVersion\\Run"
@@ -72,6 +73,7 @@
   setDefault(SETTING_LANGUAGE, LanguageSupport::defaultLanguageCode());
   setDefault(SETTING_RUN_TOR_AT_START, true);
   setDefault(SETTING_SHOW_MAINWINDOW_AT_START, true);
+  setDefault(SETTING_BROWSER_EXECUTABLE, "");
 }
 
 /** Gets the currently preferred language code for Vidalia. */
@@ -168,3 +170,19 @@
 #endif
 }
 
+/** Returns a fully-qualified path to the web browser, including the
+ * executable name. */
+QString
+VidaliaSettings::getBrowserExecutable() const
+{
+  return QDir::convertSeparators(value(SETTING_BROWSER_EXECUTABLE).toString());
+}
+
+/** Sets the location and name of the web browser executable to the given string.
+ * If set to the empty string, the browser will not be started. */
+void
+VidaliaSettings::setBrowserExecutable(const QString &browserExecutable)
+{
+  setValue(SETTING_BROWSER_EXECUTABLE, browserExecutable);
+}
+
Index: src/vidalia/config/vidaliasettings.h
===================================================================
--- src/vidalia/config/vidaliasettings.h	(revision 2205)
+++ src/vidalia/config/vidaliasettings.h	(working copy)
@@ -71,6 +71,13 @@
   bool runVidaliaOnBoot();
   /** Set whether to run Vidalia on system boot. */
   void setRunVidaliaOnBoot(bool run);
+
+  /** Returns a fully-qualified path to the web browser, including the
+   * executable name. */
+  QString getBrowserExecutable() const;
+  /** Sets the location and name of the web browser executable to the given string.
+   * If set to the empty string, the browser will not be started. */
+  void setBrowserExecutable(const QString &browserExecutable);
 };
 
 #endif
Index: src/vidalia/mainwindow.cpp
===================================================================
--- src/vidalia/mainwindow.cpp	(revision 2205)
+++ src/vidalia/mainwindow.cpp	(working copy)
@@ -152,6 +152,13 @@
   _torControl->setEvent(TorEvents::ClientStatus,  this, true);
   _torControl->setEvent(TorEvents::GeneralStatus, this, true);
 
+  /* Create a new BrowserProcess object, used to start the web browser */
+  _browserProcess = new BrowserProcess(this);
+  connect(_browserProcess, SIGNAL(finished(int, QProcess::ExitStatus)),
+	         this, SLOT(onBrowserFinished(int, QProcess::ExitStatus)));
+  connect(_browserProcess, SIGNAL(startFailed(QString)),
+	         this, SLOT(onBrowserFailed(QString)));
+
   /* Catch signals when the application is running or shutting down */
   connect(vApp, SIGNAL(running()), this, SLOT(running()));
   connect(vApp, SIGNAL(shutdown()), this, SLOT(shutdown()));
@@ -427,6 +434,35 @@
 #endif
 }
 
+/** Starts the web browser, if appropriately configured */
+void MainWindow::startBrowser(TorStatus status)
+{
+  VidaliaSettings settings;
+  QString executable = settings.getBrowserExecutable();
+  
+  if (!executable.isEmpty())
+    _browserProcess->start(executable, QStringList());
+}
+
+/** Called when browser has exited */
+void MainWindow::onBrowserFinished(int exitCode, QProcess::ExitStatus exitStatus)
+{
+  shutdown();
+}
+
+/** Called when the web browser, for example, because the path
+ * specified to the web browser executable didn't lead to an executable. */
+void
+MainWindow::onBrowserFailed(QString errmsg)
+{
+  Q_UNUSED(errmsg);
+ 
+  /* Display an error message and see if the user wants some help */
+  int response = VMessageBox::warning(this, tr("Error starting web browser"),
+				      tr("Vidalia was unable to start the configured web browser"),
+				      VMessageBox::Ok|VMessageBox::Default|VMessageBox::Escape);
+}
+
 /** Updates the UI to reflect Tor's current <b>status</b>. Returns the
  * previously set TorStatus value.*/
 MainWindow::TorStatus
@@ -1006,6 +1042,7 @@
 MainWindow::circuitEstablished()
 {
   updateTorStatus(CircuitEstablished);
+  startBrowser(CircuitEstablished);
 }
 
 /** Checks the status of the current version of Tor to see if it's old,
Index: src/vidalia/mainwindow.h
===================================================================
--- src/vidalia/mainwindow.h	(revision 2205)
+++ src/vidalia/mainwindow.h	(working copy)
@@ -49,6 +49,7 @@
 #include "help/browser/helpbrowser.h"
 #include "network/netviewer.h"
 #include "ui_mainwindow.h"
+#include "browserprocess.h"
 
 
 class MainWindow : public VidaliaWindow
@@ -116,6 +117,10 @@
   void showServerConfigDialog();
   /** Called when the "show on startup" checkbox is toggled. */
   void toggleShowOnStartup(bool checked);
+  /** Called when the web browser has stopped */
+  void onBrowserFinished(int exitCode, QProcess::ExitStatus exitStatus);
+  /** Called web the web browser failed to start */
+  void onBrowserFailed(QString errmsg);
   
 #if QT_VERSION >= 0x040200 && !defined(Q_WS_MAC)
   /** Displays the main window if <b>reason</b> is DoubleClick. */
@@ -147,6 +152,8 @@
   /** Updates the UI to reflect Tor's current <b>status</b>. Returns the
    * previously set TorStatus value. */
   TorStatus updateTorStatus(TorStatus status);
+  /** Starts the web browser, if appropriately configured */
+  void startBrowser(TorStatus status);
   /** Converts a TorStatus enum value to a string for debug logging purposes. */
   QString toString(TorStatus status);
   /** Authenticates Vidalia to Tor's control port. */
@@ -184,6 +191,8 @@
   ConfigDialog* _configDialog;
   /** A TorControl object that handles communication with Tor */
   TorControl* _torControl;
+  /** A BrowserProcess object that manages the web browser */
+  BrowserProcess* _browserProcess;
   /** Remembers the control password between when we start Tor with a hash of
    * the password and when we need to provide the password itself. */
   QString _controlPassword;
